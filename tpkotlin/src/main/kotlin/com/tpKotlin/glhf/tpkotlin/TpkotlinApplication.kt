package com.tpKotlin.glhf.tpkotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan
open class TpkotlinApplication: SpringBootServletInitializer(){

	override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
		return application.sources(org.springframework.boot.autoconfigure.SpringBootApplication::class.java)
	}
}

fun main(args: Array<String>) {
	runApplication<TpkotlinApplication>(*args)
}